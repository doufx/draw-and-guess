package com.qibiao.drawguess;
import com.qibiao.drawguess.slice.CrossDeviceMigrationFeatureSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.IAbilityContinuation;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;

/**
*
* @description 可迁移的page，需要实现IAbilityContinuation接口。同时，此Page所包含的所有AbilitySlice也需要实现此接口。
* @author wwx978768
* @date 2020/9/22 17:08
*
*/
public class MigrationFeatureAbility extends Ability implements IAbilityContinuation {

    private static final String TAG = MigrationFeatureAbility.class.getSimpleName();

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(CrossDeviceMigrationFeatureSlice.class.getName());
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
    * 跨设备迁移需要实现的接口回调
    * */
    @Override
    public boolean onStartContinuation() {
        LogUtil.info(TAG, "开始Ability迁移");
        return true;
    }


    @Override
    public boolean onSaveData(IntentParams intentParams) {
        LogUtil.info(TAG, "开始保存Ability数据");
        return true;
    }

    @Override
    public boolean onRestoreData(IntentParams intentParams) {
        LogUtil.info(TAG, "开始onRestoreData");
        return true;
    }

    @Override
    public void onCompleteContinuation(int i) {
        LogUtil.info(TAG, "Ability 迁移完成 i:"+i);
    }

    @Override
    public void onRemoteTerminated() {
        LogUtil.info(TAG, "Ability onRemoteTerminated");
    }

}
