package com.qibiao.drawguess.slice;

import com.qibiao.drawguess.Const;
import com.qibiao.drawguess.LogUtil;
import com.qibiao.drawguess.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.bundle.ElementName;

public class MainAbilitySlice extends AbilitySlice {
    private static final String TAG = MainAbilitySlice.class.getSimpleName();
    private Component starBtn;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        LogUtil.info(TAG, "is onStart begin");
        initView();
        LogUtil.info(TAG, "is onStart end");
    }

    private void initView() {
        Component rootView = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_main_ability_slice, null, false);
        setClickAction(rootView);
        super.setUIContent((ComponentContainer) rootView);
    }

    private void setClickAction(Component rootView) {
        starBtn = rootView.findComponentById(ResourceTable.Id_start_draw_page_btn);
        starBtn.setClickedListener(mClickListener);
    }

    private Component.ClickedListener mClickListener = new Component.ClickedListener() {
        @Override
        public void onClick(Component component) {
            LogUtil.info(TAG, " onClick component: " + component.toString());
            int componentId = component.getId();
            Intent intent = new Intent();
            ElementName elementName = new ElementName();
            elementName.setDeviceId("");
            elementName.setBundleName(Const.BUNDLE_NAME);
            switch (componentId) {
                case ResourceTable.Id_start_draw_page_btn:
                    LogUtil.info(TAG, "Go Study WidgetUseAbility.");
                    elementName.setAbilityName("MigrationFeatureAbility");
                    intent.setElement(elementName);
                    startAbility(intent);
                    break;

                default:
                    LogUtil.info(TAG, "not found component id.");
                    break;
            }
        }
    };
}
