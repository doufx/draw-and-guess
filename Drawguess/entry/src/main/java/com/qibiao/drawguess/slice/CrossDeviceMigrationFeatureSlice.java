package com.qibiao.drawguess.slice;
import com.qibiao.drawguess.DeviceUtils;
import com.qibiao.drawguess.Draw;
import com.qibiao.drawguess.LogUtil;
import com.qibiao.drawguess.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.IAbilityContinuation;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.DirectionalLayout.LayoutConfig;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextAlignment;
import java.util.List;

/**
* @ClassName   CrossDeviceMigrationFeatureSlice
* @description   可迁移的FA，需要实现IAbilityContinuation接口，主要包含迁移FA，可回迁FA，回迁FA 功能
* @author wwx978768
* @date 2020/9/18 16:05
*/
public class CrossDeviceMigrationFeatureSlice extends AbilitySlice implements IAbilityContinuation {
    private static final String TAG = CrossDeviceMigrationFeatureSlice.class.getSimpleName();
    private Button crossDevicePageBtn;

    private TextField migrationContext;
    private  TextField synText;
    private  Draw drawComponet;


    Component rootView;

    String textString = "输入答案……";
    String pictureString = "";

    Path path;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        LogUtil.info(TAG, "执行onStart");
        InitDraw();
    }


    private  void InitDraw()
    {
        DirectionalLayout directionalLayout = new DirectionalLayout(this);
        LayoutConfig config = new LayoutConfig(LayoutConfig.MATCH_PARENT, LayoutConfig.MATCH_PARENT);
        directionalLayout.setLayoutConfig(config);
        directionalLayout.setOrientation(Component.VERTICAL);
        drawComponet = new Draw(this);
        drawComponet.setLayoutConfig(config);
        ShapeElement element = new ShapeElement();
        element.setRgbColor(new RgbColor(255, 0, 0));
        drawComponet.setBackground(element);

        if(pictureString!=null && pictureString.length()>0){
            // 恢复画图数据
            drawComponet.readPictureString(pictureString);
        }

        Button clearBtn = BtnCreator("擦除");
        ShapeElement clearelement = new ShapeElement();
        clearelement.setRgbColor(new RgbColor(125, 125, 125));
        clearBtn.setBackground(clearelement);
        clearBtn.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                drawComponet.Clear();
                drawComponet.ClearData();
            }
        });

        synText = new TextField(getContext());
        synText.setHeight(100);
        synText.setWidth(ComponentContainer.LayoutConfig.MATCH_PARENT);
        synText.setTextSize(80);
        synText.setTextAlignment(TextAlignment.HORIZONTAL_CENTER);
        synText.setText(textString);

        ShapeElement synlement = new ShapeElement();
        synlement.setRgbColor(new RgbColor(2, 2, 255));
        Button synButton = BtnCreator("我猜");
        synButton.setBackground(synlement);
        synButton.setTextColor(Color.WHITE);
        synButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                                try {
                    String deviceId = getDeviceId();
                    LogUtil.info(TAG, "请求迁移continueAbility deviceId:"+deviceId);
                    continueAbility(deviceId);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        directionalLayout.addComponent(clearBtn);
        directionalLayout.addComponent(synButton);
        directionalLayout.addComponent(synText);
        directionalLayout.addComponent(drawComponet);

        super.setUIContent(directionalLayout);
    }

    private  Button BtnCreator(String text)
    {
        Button button = new Button(getContext());
        button.setHeight(ComponentContainer.LayoutConfig.MATCH_CONTENT);
        button.setWidth(ComponentContainer.LayoutConfig.MATCH_PARENT);

        button.setText(text);
        button.setTextSize(80);
        button.setTextAlignment(TextAlignment.HORIZONTAL_CENTER);
        return button;
    }



    private Component.ClickedListener mClickedListener = new Component.ClickedListener() {
        @Override
        public void onClick(Component component) {
            String deviceId = getDeviceId();
            switch (component.getId()){

                case ResourceTable.Id_cross_device_page_btn:
                    try {
                        LogUtil.info(TAG, "请求迁移continueAbility deviceId:"+deviceId);
                        continueAbility(deviceId);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    break;
            }
        }
    };

    /**
    * @Param []
    * @description  获取当前组网下可迁移的设备id
    * @author wwx978768
    * @date 2020/9/22 16:00
    * @return java.lang.String
    */
    private String getDeviceId(){
        String deviceId = "";
        List<String> outerDevices = DeviceUtils.getAvailableDeviceId();
        LogUtil.info(TAG, "getDeviceId DeviceUtils.getRemoteDevice() = " + outerDevices);
        if (outerDevices == null || outerDevices.size() == 0){
            LogUtil.error(TAG, "no other device to continue");
        }else {
            for (String item : outerDevices) {
                LogUtil.info(TAG, "item deviceId = " + item);
            }
            deviceId = outerDevices.get(0);
        }
        LogUtil.info(TAG, "continueAbility to deviceId = " + deviceId);
        return deviceId;
    };

    /**
    * @Param []
    * @description Page请求迁移后，系统首先回调此方法，开发者可以在此回调中决策当前是否可以执行迁移
    * @author wwx978768
    * @date 2020/9/22 16:05
    * @return boolean
    */
    @Override
    public boolean onStartContinuation() {
        LogUtil.info(TAG, "开始Slice迁移onStartContinuation");
        return true;
    }

    /**
    * @Param [intentParams] 存储保存数据对象
    * @description  如果onStartContinuation()返回true，则系统回调此方法，开发者在此回调中保存必须传递到另外设备上以便恢复Page状态的数据
    * @author wwx978768
    * @date 2020/9/18 15:48
    * @return boolean
    */
    @Override
    public boolean onSaveData(IntentParams intentParams) {
        try {
            intentParams.setParam("data",synText.getText());
            pictureString = drawComponet.getPictureData();
            intentParams.setParam("picture", pictureString);
            LogUtil.info(TAG, "数据保存>>>>>>>" +pictureString);
        }catch (Exception e){
            e.printStackTrace();
        }
        return true;
    }

    /**
    * @Param [intentParams] 源端传递过来的参数
    * @description 源侧设备上Page完成保存数据后，系统在目标侧设备上回调此方法，开发者在此回调中接受用于恢复Page状态的数据,对端设备会在onStart 生命周期之前执行该函数
    * @author wwx978768
    * @date 2020/9/18 11:11
    * @return boolean
    */
    @Override
    public boolean onRestoreData(IntentParams intentParams) {
        try {

            textString = (String)intentParams.getParam("data");
            pictureString = (String)intentParams.getParam("picture");
            LogUtil.info(TAG, "数据还原>>>>>>>" +pictureString);
            LogUtil.info(TAG, "目标侧接收迁移数据onRestoreData"+textString);
        }catch (Exception e){
            e.printStackTrace();
            LogUtil.info(TAG, "发生错误"+e.toString());
        }
        return true;
    }

    /**
    * @Param [i] 表示迁移结果
    * @description 目标侧设备上恢复数据一旦完成，系统就会在源侧设备上回调Page的此方法，以便通知应用迁移流程已结束
    * @author wwx978768
    * @date 2020/9/22 16:02
    * @return void
    */
    @Override
    public void onCompleteContinuation(int i) {
        LogUtil.info(TAG, "Slice 迁移完成");
    }

    /**
    * @Param []
    * @description 如果目标侧设备上Page因任何原因终止，则源侧Page通过此回调接收终止通知
    * @author wwx978768
    * @date 2020/9/22 16:03
    * @return void
    */
    @Override
    public void onRemoteTerminated() {
        LogUtil.info(TAG, "远程 Slice onRemoteTerminated");
    }
}
