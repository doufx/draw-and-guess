/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2020-2020. All rights reserved.
 * Description: Const vars
 * Create: 2020-01-06
 */

package com.qibiao.drawguess;

/**
 * Define some common parameters
 *
 * @since 2019-10-15
 */
public class Const {
    /**
     * bundle name
     */
    public static final String BUNDLE_NAME = "com.qibiao.drawguess";

    /**
     * App name
     */
    public static final String APP_NAME = "HiMyAce";


    private Const() {
        /* Do nothing */
    }
}
