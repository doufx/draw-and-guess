package com.qibiao.drawguess;

import com.qibiao.drawguess.slice.MainAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());

        requestPermissionsFromUser(
                new String[]{
                        "ohos.permission.DISTRIBUTED_DATASYNC",
                        "ohos.permission.GET_DISTRIBUTED_DEVICE_INFO",
                        "ohos.permission.servicebus.ACCESS_SERVICE"
                }, 0);
    }
}
