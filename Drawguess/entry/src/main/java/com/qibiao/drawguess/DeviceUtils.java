/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2020-2020. All rights reserved.
 */

package com.qibiao.drawguess;
import ohos.account.AccountAbility;
import ohos.account.DistributedInfo;
import ohos.app.AbilityContext;
import ohos.app.Context;
import ohos.distributedschedule.interwork.DeviceInfo;
import ohos.distributedschedule.interwork.DeviceManager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


/**
 * Device method utils
 *
 * @since 2020-01-17
 */
public class DeviceUtils {
    private static final String TAG = DeviceUtils.class.getSimpleName();

    private DeviceUtils() {
        // do nothing
    }

    /**
     * Get group id.
     *
     * @return group id.
     * @since 2020-01-12
     */
    public static String getGroupId() {
        AccountAbility account = AccountAbility.getAccountAbility();
        DistributedInfo distributeInfo = account.queryOsAccountDistributedInfo();
        return distributeInfo.getId();
    }

    /**
     * Get available id but not contain self.
     *
     * @return available device ids
     * @since 2020-01-12
     */
    public static List<String> getAvailableDeviceId() {
        LogUtil.info(TAG, "begin getAvailableDeviceId");
        List<String> deviceIds = new ArrayList<>();
        List<DeviceInfo> deviceInfoList = DeviceManager.getDeviceList(DeviceInfo.FLAG_GET_ONLINE_DEVICE);
        if (deviceInfoList == null) {
            return deviceIds;
        }
        LogUtil.info(TAG, "deviceInfoList size " + deviceInfoList.size());
//        String localDeviceId = getLocalDeviceId();
        if (deviceInfoList.size() == 0) {
//        if (deviceInfoList.size() == 0 || !sDistributedLoginStateinstance.isLoginIn(localDeviceId)) {
            LogUtil.info(TAG, "did not find other device");
            return deviceIds;
        }

        for (DeviceInfo deviceInfo : deviceInfoList) {
            deviceIds.add(deviceInfo.getDeviceId());
        }
        LogUtil.info(TAG, "deviceInfoList end.... ");
        return deviceIds;
    }
}
